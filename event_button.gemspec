$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "event_button/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "event_button"
  s.version     = EventButton::VERSION
  s.authors     = ["yongoh"]
  s.email       = ["a.yongoh@gmail.com"]
  s.homepage    = ""
  s.summary     = ""
  s.description = ""
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "activesupport"
  s.add_dependency 'core_ext'

  s.add_development_dependency "rails", "~> 5.0.7", ">= 5.0.7.1"
  s.add_development_dependency "sqlite3", "~> 1.3.13"
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'rspec-html-matchers'
  s.add_development_dependency 'capybara'
  s.add_development_dependency 'poltergeist'
  s.add_development_dependency 'puma'
  s.add_development_dependency 'coffee-rails'
  s.add_development_dependency 'sass-rails'
end
