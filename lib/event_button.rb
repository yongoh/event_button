require "event_button/engine"
require "event_button/builder"

module EventButton

  class << self

    # @param [String,Symbol] name イベントボタン名
    # @return [String] 翻訳したイベントボタン名
    def humanize_name(name)
      I18n.t(name, scope: [:event_button])
    end
  end
end
