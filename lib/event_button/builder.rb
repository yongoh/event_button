module EventButton
  class Builder

    class << self

      private

      # イベントボタン要素生成メソッドを定義
      # @param [Symbol] name イベントボタン名
      # @param [String] format メソッド名のフォーマット
      # @param [Hash] options イベントボタン要素生成メソッドに渡すオプションのデフォルト値
      def event_button(name, format: "%{name}_button", **options)
        method_name = I18n.t(default: format, name: name)

        # @return [ActiveSupport::SafeBuffer] `name`イベントボタン要素
        define_method(method_name) do |*args, &block|
          args.push(options.merge(args.pop)) if args.last.is_a?(Hash)
          event_button(name, *args, &block)
        end
      end
    end

    # @param [ActionView::Base] template ビューテンプレート
    def initialize(template)
      @template = template
    end

    # @param [String,Symbol] name イベントボタン名
    # @param [String] content イベントボタン要素の内容
    # @param [Boolean] highlight 操作対象をハイライトするかどうか
    # @param [Hash] html_attributes HTML属性
    # @yield [] ブロックの返り値を内容にする。`content`より優先。
    # @return [ActiveSupport::SafeBuffer] JavaScriptイベント起動用ボタン要素
    def event_button(name, content = EventButton.humanize_name(name), highlight: true, **html_attributes, &content_proc)
      content = h.capture(&content_proc) if block_given?
      html_attributes = {class: "event_button", data: {event: name}}.html_attribute_merge(html_attributes)
      html_attributes.html_attribute_merge!(class: "event_button-highlightable") if highlight
      h.content_tag(:button, content, html_attributes)
    end

    private

    def h
      @template
    end
  end
end
