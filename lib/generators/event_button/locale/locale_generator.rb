module EventButton
  class LocaleGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)

    argument :locales, type: :array, default: %i(en ja), banner: "locale locale"

    def copy_locale_files
      locales.each do |locale|
        # HACK: 他にテンプレートファイルに変数を渡す方法は無いものか
        define_singleton_method(:locale){ locale }
        template "locale.yml.erb", "config/locales/event_button/#{locale}.yml"
      end
    end
  end
end
