module EventButton
  module ApplicationHelper

    # @return [ActiveSupport::SafeBuffer] JavaScriptイベント起動用ボタン要素
    def event_button(*args, &block)
      Builder.new(self).event_button(*args, &block)
    end
  end
end
