# 操作対象ハイライトイベントリスナー
document.addEventButtonListener "mouseover", (button)->
  button.highlight(true) if button.isHighlightable()

document.addEventButtonListener "mouseout", (button)->
  button.highlight(false) if button.isHighlightable()
