class window.EventButton

  # @!attribute [rw] event
  #   @return [Event] イベントオブジェクト
  # @!attribute [rw] options
  #   @return [Hash] オプション
  # @!attribute [rw] name
  #   @return [String] イベントボタン名

  # @param [Event] event イベントオブジェクト
  # @param [Hash] options オプション
  # @option options [String,Function,HTMLElement] for イベント操作対象のデフォルト
  constructor: (event, options = {})->
    @event = event
    @options = options
    @name = @elementOfPower().dataset.event

  # @return [HTMLElement] 起点となるイベントが発生した要素
  elementOfPower: ->
    @event.target

  # @return [HTMLElement] イベントによる操作の対象となる要素
  # @note 優先度
  #   1. カスタムイベントボタンクラスのオーバーライドした`#elementOfAction`
  #   2. イベントボタン要素の`for`属性
  #   3. `@options.for`
  elementOfAction: ->
    result = @elementOfPower().getAttribute("for") || @options.for
    switch typeof result
      when "string" then document.getElementById(result)
      when "function" then result.call(this)
      else result

  # 操作対象をハイライト
  # @param [Boolean] bool `true`ならハイライト指標をつける、`false`ならはずす。
  highlight: (bool = true)->
    if elm = @elementOfAction()
      elm.classList.toggle(EventButton.HTML_CLASSES.HIGHLIGHT, bool)

  # @return [Boolean] 操作対象をハイライトするかどうか
  isHighlightable: ->
    @elementOfPower().classList.contains(EventButton.HTML_CLASSES.HIGHLIGHTABLE)

EventButton.HTML_CLASSES =
  # 操作対象要素のハイライト指標
  HIGHLIGHT: "event_button-highlight"
  # イベントボタンのハイライト指標
  HIGHLIGHTABLE: "event_button-highlightable"

EventButton.SELECTORS =
  BUTTON: ".event_button"

  # @param [String] name イベントボタン名
  # @return [String] 特定のイベントボタンのセレクタ
  button: (name)->
    "#{EventButton.SELECTORS.BUTTON}[data-event='#{name}']"
