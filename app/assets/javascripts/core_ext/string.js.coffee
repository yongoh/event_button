String.prototype.camelize = ->
  @replace(/^./g, (s)-> s.toUpperCase()).replace(/_./g, (s)-> s.charAt(1).toUpperCase())

String.prototype.underscore = ->
  @replace(/[A-Z]/g, (s)-> '_' + s.toLowerCase()).replace(/^_/, '')

String.prototype.classify = ->
  @split("/").map((key)-> key.camelize()).join(".")

String.prototype.constantize = ->
  @split(".").reduce(((obj, name)-> obj[name]), window)
