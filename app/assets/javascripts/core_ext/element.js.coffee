# イベントボタンにイベントリスナーをセット
# @overload addEventButtonListener(eventType, name, options, func)
# @overload addEventButtonListener(eventType, name, func)
# @overload addEventButtonListener(eventType, func)
# @overload addEventButtonListener(eventType, options, func)
# @param [String] eventType イベントの種類
# @param [String,null] name イベントボタン名。`null`もしくは省略した場合は全てのイベントボタンにイベントリスナーをセットする。
# @param [Hash] options オプション
# @option options [Class,String] class イベントボタンクラス。デフォルトは`EventButton`。
# @yield [button] イベント処理を記述するコールバック関数。必須。
# @yieldparam [EventButton] button イベントボタンオブジェクト
addEventButtonListener = (eventType, args...)->
  [selector, options, func] = variadic(args...)
  @addEventListener eventType, (event)->
    return unless event.target.matches(selector)
    event.preventDefault()
    klass = options.class || event.target.dataset.jsClass || EventButton
    klass = klass.constantize() if typeof klass == "string"
    func(new klass(event, options))

# 可変長引数の整理
variadic = (name, options, func)->
  if name
    [selector, options, func] =
      switch typeof name
        when "function" then [EventButton.SELECTORS.BUTTON, {}, name]
        when "object" then [EventButton.SELECTORS.BUTTON, name, options]
        when "string" then [EventButton.SELECTORS.button(name), options, func]
  else
    selector = EventButton.SELECTORS.BUTTON
  if typeof options == "function"
    [options, func] = [{}, options]
  [selector, options, func]

Document.prototype.addEventButtonListener = addEventButtonListener
Element.prototype.addEventButtonListener = addEventButtonListener

# https://developer.mozilla.org/ja/docs/Web/API/Element/matches#Polyfill
Element.prototype.matches ?=
  Element.prototype.matchesSelector ||
  Element.prototype.mozMatchesSelector ||
  Element.prototype.msMatchesSelector ||
  Element.prototype.oMatchesSelector ||
  Element.prototype.webkitMatchesSelector

# https://developer.mozilla.org/ja/docs/Web/API/Element/closest#Polyfill
Element.prototype.closest ?= (selector)->
  elm = this
  while elm != null && elm.nodeType == 1
    return elm if elm.matches(selector)
    elm = elm.parentElement || elm.parentNode
  null
