require 'rails_helper'

describe EventButton::ApplicationHelper do
  describe "#event_button" do
    it "は、イベント起動用ボタン要素を返すこと" do
      expect(h.event_button(:alert)).to have_tag("button.event_button", with: {"data-event" => "alert"}, text: "アラート")
    end
  end
end
