require 'rails_helper'

feature "rails generate event_button:locale" do
  before do
    Dir.chdir(Rails.root)
    `rails g event_button:locale #{command_arguments.join(" ")}`
  end

  let(:command_arguments){ %w(de en ja) }

  after do
    FileUtils.rm_r(locale_root_path) rescue Errno::ENOENT
  end

  let(:locale_root_path){ "config/locales/event_button" }

  def yaml_path(locale)
    "#{locale_root_path}/#{locale}.yml"
  end

  scenario "ロケールファイルを生成" do
    command_arguments.each do |arg|
      path = yaml_path(arg)
      expect(File).to be_exist(path)
      expect(File.read(path)).to match(/^#{arg}:$/)
    end
  end

  feature "`destroy`コマンドを実行" do
    before do
      `rails d event_button:locale #{command_arguments.join(" ")}`
    end

    scenario "生成したロケールファイルを削除" do
      command_arguments.each do |arg|
        expect(File).not_to be_exist(yaml_path(arg))
      end
    end
  end
end
