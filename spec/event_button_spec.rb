require 'rails_helper'

describe EventButton do
  describe "::humanize_name" do
    it "は、翻訳したイベントボタン名を返すこと" do
      expect(described_class.humanize_name(:add)).to eq("追加")
    end
  end
end
