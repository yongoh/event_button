describe "Element", ->
  beforeEach ->
    loadFixtures("event_button.html")
    @parent = document.getElementById("parent")
    @button = document.getElementById("event_button-1")

  describe "#addEventButtonListener", ->
    describe "第1引数にイベントの種類・第2引数にイベントボタン名・第3引数に空のオプション・第4引数にコールバックを渡した場合", ->
      describe "コールバック", ->
        afterEach ->
          @button.click()

        it "は、コールバック関数の第1引数にイベントボタンオブジェクトを渡すこと", ->
          @parent.addEventButtonListener "click", "alert", {}, (eventButton)=>
            expect(eventButton).toEqual(jasmine.any(EventButton))
            expect(eventButton.event.type).toBe("click")
            expect(eventButton.event.target).toEqual(@button)
            expect(eventButton.name).toBe("alert")

      beforeEach ->
        @parent.addEventButtonListener "click", "alert", {}, (eventButton)->
          alert("あらーと！")

      describe "イベント登録前からあるボタン要素をクリックした場合", ->
        it "は、登録したイベント処理を実行すること", ->
          spyOn(window, "alert")
          @button.click()
          expect(window.alert).toHaveBeenCalledWith("あらーと！")

      describe "イベント登録後に設置したボタン要素をクリックした場合", ->
        beforeEach ->
          @button2 = document.createElement("button")
          @button2.classList.add("event_button")
          @button2.dataset.event = "alert"
          @parent.appendChild(@button2)

        it "は、登録したイベント処理を実行すること", ->
          spyOn(window, "alert")
          @button2.click()
          expect(window.alert).toHaveBeenCalledWith("あらーと！")

      describe "別のイベントボタン名のボタン要素をクリックした場合", ->
        beforeEach ->
          @button3 = document.getElementById("event_button-2")

        it "は、登録したイベント処理を実行しないこと", ->
          spyOn(window, "alert")
          @button3.click()
          expect(window.alert).not.toHaveBeenCalled()

    describe "カスタムイベントボタンクラス", ->
      beforeEach ->
        @func = (eventButton)=>
          expect(eventButton).toEqual(jasmine.any(CustomEventButton))
          expect(eventButton.event.type).toBe("click")
          expect(eventButton.event.target).toEqual(@button)
          expect(eventButton.name).toBe("alert")
          expect(eventButton.hoge).toBe("Hoge!")

      afterEach ->
        @button.click()

      describe "第3引数のオプション'class'にカスタムイベントボタンクラスを渡した場合", ->
        it "コールバックは、第1引数にオプションで渡したクラスのインスタンスを渡すこと", ->
          @parent.addEventButtonListener("click", "alert", class: CustomEventButton, @func)

      describe "第3引数のオプション'class'にクラス名を渡した場合", ->
        it "コールバックは、第1引数にオプションで渡した名前のクラスのインスタンスを渡すこと", ->
          @parent.addEventButtonListener("click", "alert", class: "CustomEventButton", @func)

      describe "ボタン要素の`data-js-class`属性値と同じ名前のクラスがある場合", ->
        beforeEach ->
          @button.dataset.jsClass = "CustomEventButton"

        it "コールバックは、第1引数にそのクラスのインスタンスを渡すこと", ->
          @parent.addEventButtonListener("click", "alert", {}, @func)

    describe "第2引数に`null`を渡した場合", ->
      beforeEach ->
        @parent.addEventButtonListener "click", null, {}, (eventButton)->
          alert("あらーと！")

      it "は、名前に関わらず全てのイベントボタンにイベントをセットすること", ->
        spyOn(window, "alert")
        @button.click()
        expect(window.alert).toHaveBeenCalledWith("あらーと！")

      it "は、名前に関わらず全てのイベントボタンにイベントをセットすること", ->
        button = document.getElementById("event_button-2")

        spyOn(window, "alert")
        button.click()
        expect(window.alert).toHaveBeenCalledWith("あらーと！")

      it "は、名前の無いイベントボタンにもイベントをセットすること", ->
        button = document.getElementById("event_button-2")
        button.dataset.event = null

        spyOn(window, "alert")
        button.click()
        expect(window.alert).toHaveBeenCalledWith("あらーと！")

    describe "第3引数を省略した場合", ->
      beforeEach ->
        @parent.addEventButtonListener "click", "alert", (eventButton)->
          alert("あらーと！")

      it "は、第2引数に渡した名前のイベントボタンにイベントをセットすること", ->
        spyOn(window, "alert")
        @button.click()
        expect(window.alert).toHaveBeenCalledWith("あらーと！")

    describe "第2引数を省略した場合", ->
      beforeEach ->
        @parent.addEventButtonListener "click", {}, (eventButton)->
          alert("あらーと！")

      it "は、全てのイベントボタンにイベントをセットすること", ->
        spyOn(window, "alert")
        @button.click()
        expect(window.alert).toHaveBeenCalledWith("あらーと！")

    describe "第2・第3引数を省略した場合", ->
      beforeEach ->
        @parent.addEventButtonListener "click", (eventButton)->
          alert("あらーと！")

      it "は、全てのイベントボタンにイベントをセットすること", ->
        spyOn(window, "alert")
        @button.click()
        expect(window.alert).toHaveBeenCalledWith("あらーと！")

  describe "#matches", ->
    it "は、自身が渡したセレクタ文字列に一致するかどうかを判定すること", ->
      expect(@button.matches(".event_button[for='element-of-action-1']")).toBe(true)
      expect(@button.matches(".event_button[for='element-of-action-2']")).toBe(false)

  describe "#closest", ->
    beforeEach ->
      loadFixtures("closest.html")
      @div = document.querySelector(".eee")

    describe "渡したセレクタ文字列に一致する先祖がある場合", ->
      it "は、その要素オブジェクトを返すこと", ->
        result = @div.closest(".bbb")
        expect(result).toEqual(jasmine.any(HTMLDivElement))
        expect(result).toEqual(".bbb#b-3")

    describe "渡したセレクタ文字列に一致する先祖が無い場合", ->
      it "は、`null`を返すこと", ->
        expect(@div.closest(".zzz")).toBeNull()
