describe "String", ->
  describe "#camelize", ->
    it "は、自身（文字列）をキャメルケースに変換すること", ->
      expect("event_button".camelize()).toBe("EventButton")

  describe "#underscore", ->
    it "は、自身（文字列）をスネークケースに変換すること", ->
      expect("EventButton".underscore()).toBe("event_button")

  describe "#classify", ->
    it "は、自身（文字列）をクラス名の形に変換すること", ->
      expect("hoge/piyo/foo_bar_baz".classify()).toBe("Hoge.Piyo.FooBarBaz")

  describe "#constantize", ->
    describe "自身（文字列）の名前の定数が存在する場合", ->
      it "は、その定数の値を取得すること", ->
        expect("EventButton.SELECTORS.BUTTON".constantize()).toBe(EventButton.SELECTORS.BUTTON)

    describe "自身（文字列）の名前の定数が存在しない場合", ->
      it "は、`undefined`を返すこと", ->
        expect("EventButton.SELECTORS.UNKNOWN".constantize()).toBeUndefined()
