describe "EventButton", ->
  beforeEach ->
    loadFixtures("event_button.html")

    @button = document.querySelector(".event_button[data-event='alert']")
    @eventButton = new EventButton({target: @button})

  describe "@name", ->
    it "は、イベントボタン名を返すこと", ->
      expect(@eventButton.name).toBe("alert")

  describe "#elementOfAction", ->
    describe "ボタン要素が`for`属性を持たない場合", ->
      beforeEach ->
        @button.removeAttribute("for")

      describe "`@options.for`が存在しない場合", ->
        it "は、`undefined`を返すこと", ->
          expect(@eventButton.elementOfAction()).toBeUndefined()

      describe "`@options.for`が文字列の場合", ->
        beforeEach ->
          @eventButton = new EventButton({target: @button}, {for: "element-of-action-2"})

        describe "`@options.for`と同じ`id`属性値を持つ要素が存在しない場合", ->
          beforeEach ->
            document.getElementById("element-of-action-2").remove()

          it "は、`null`を返すこと", ->
            expect(@eventButton.elementOfAction()).toBeNull()

        describe "`@options.for`と同じ`id`属性値を持つ要素が存在する場合", ->
          it "は、該当する要素を返すこと", ->
            result = @eventButton.elementOfAction()
            expect(result).toEqual(jasmine.any(HTMLDivElement))
            expect(result).toEqual("div#element-of-action-2")

      describe "`@options.for`が関数の場合", ->
        beforeEach ->
          @eventButton = new EventButton({target: @button}, {for: -> @name})

        it "は、関数の結果を返すこと", ->
          expect(@eventButton.elementOfAction()).toBe(@eventButton.name)

      describe "`@options.for`が要素の場合", ->
        beforeEach ->
          @eventButton = new EventButton({target: @button}, {for: document.getElementById("element-of-action-2")})

        it "は、その要素を返すこと", ->
          result = @eventButton.elementOfAction()
          expect(result).toEqual(jasmine.any(HTMLDivElement))
          expect(result).toEqual("div#element-of-action-2")

    describe "ボタン要素が`for`属性を持つ場合", ->
      describe "ボタンの`for`属性値と同じ`id`属性値を持つ要素が存在しない場合", ->
        beforeEach ->
          document.getElementById("element-of-action-1").remove()

        it "は、`null`を返すこと", ->
          expect(@eventButton.elementOfAction()).toBeNull()

      describe "ボタンの`for`属性値と同じ`id`属性値を持つ要素が存在する場合", ->
        it "は、該当する要素を返すこと", ->
          result = @eventButton.elementOfAction()
          expect(result).toEqual(jasmine.any(HTMLDivElement))
          expect(result).toEqual("div#element-of-action-1")

  describe "#highlight", ->
    describe "`true`を渡した場合 (default)", ->
      it "は、操作対象にハイライト指標をつけること", ->
        @eventButton.highlight()
        expect("#element-of-action-1").toHaveClass(EventButton.HTML_CLASSES.HIGHLIGHT)

    describe "`false`を渡した場合", ->
      beforeEach ->
        document.getElementById("element-of-action-1").classList.add(EventButton.HTML_CLASSES.HIGHLIGHT)

      it "は、操作対象からハイライト指標をはずすこと", ->
        @eventButton.highlight(false)
        expect("#element-of-action-1").not.toHaveClass(EventButton.HTML_CLASSES.HIGHLIGHT)

    itBehavesLikeNoError = ->
      it "は、エラーを発生させないこと", ->
        func = =>
          @eventButton.highlight()
        expect(func).not.toThrow()

    describe "操作対象要素が存在しない場合", ->
      beforeEach ->
        document.querySelector("#element-of-action-1").remove()

      itBehavesLikeNoError.call(this)

    describe "イベントボタンに属性`for`が存在しない場合", ->
      beforeEach ->
        @button.removeAttribute("for")

      itBehavesLikeNoError.call(this)

  describe "::SELECTORS.button", ->
    describe "イベントボタン名を渡した場合", ->
      it "は、そのイベントを起動するイベントボタン要素のセレクタを返すこと", ->
        expect(EventButton.SELECTORS.button("piyo")).toBe(".event_button[data-event='piyo']")
