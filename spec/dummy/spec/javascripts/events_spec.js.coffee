describe "操作対象ハイライトイベントリスナー", ->
  beforeEach ->
    loadFixtures("event_button.html")
    @button = document.getElementById("event_button-1")

    # イベント
    @mouseoverEvent = document.createEvent("MouseEvents")
    @mouseoverEvent.initEvent("mouseover", true, true)

    @mouseoutEvent = document.createEvent("MouseEvents")
    @mouseoutEvent.initEvent("mouseout", true, true)

  describe "イベントボタンに操作対象ハイライト指標が無い場合", ->
    describe "イベントボタンをマウスオーバーした場合", ->
      it "は、操作対象要素は変化しないこと", ->
        @button.dispatchEvent(@mouseoverEvent)
        expect("#element-of-action-1").not.toHaveClass(EventButton.HTML_CLASSES.HIGHLIGHT)

    describe "イベントボタンからマウスアウトした場合", ->
      it "は、操作対象要素は変化しないこと", ->
        @button.dispatchEvent(@mouseoutEvent)
        expect("#element-of-action-1").not.toHaveClass(EventButton.HTML_CLASSES.HIGHLIGHT)

  describe "イベントボタンに操作対象ハイライト指標がある場合", ->
    beforeEach ->
      @button.classList.add(EventButton.HTML_CLASSES.HIGHLIGHTABLE)

    describe "イベントボタンにマウスオーバーした場合", ->
      it "は、操作対象要素をハイライトすること", ->
        @button.dispatchEvent(@mouseoverEvent)
        expect("#element-of-action-1").toHaveClass(EventButton.HTML_CLASSES.HIGHLIGHT)

    describe "イベントボタンからマウスアウトした場合", ->
      beforeEach ->
        document.getElementById("element-of-action-1").classList.add(EventButton.HTML_CLASSES.HIGHLIGHT)

      it "は、操作対象要素からハイライトを消すこと", ->
        @button.dispatchEvent(@mouseoutEvent)
        expect("#element-of-action-1").not.toHaveClass(EventButton.HTML_CLASSES.HIGHLIGHT)