require 'rails_helper'

describe EventButton::Builder do
  let(:builder){ described_class.new(view_context) }

  describe "::event_button" do
    let(:builder){ builder_class.new(view_context) }

    context "イベントボタン名だけを渡した場合" do
      let(:builder_class){
        Class.new(described_class) do
          event_button :alert
        end
      }

      it "は、渡した名前のイベント用ボタンを返すメソッドを生成すること" do
        expect(builder.alert_button).to have_tag("button.event_button", with: {"data-event" => "alert"}, text: "アラート")
      end
    end

    context "オプション`:format`を渡した場合" do
      let(:builder_class){
        Class.new(described_class) do
          event_button :alert, format: "aaa_%{name}_bbb"
        end
      }

      it "は、フォーマット通りの名前のメソッドを生成すること" do
        expect(builder.aaa_alert_bbb).to have_tag("button.event_button", with: {"data-event" => "alert"}, text: "アラート")
        expect(builder.methods).not_to be_include(:alert_button)
      end
    end
  end

  describe "#event_button" do
    subject do
      builder.event_button(:alert)
    end

    it "は、イベント起動用ボタン要素を返すこと" do
      is_expected.to have_tag("button.event_button", with: {"data-event" => "alert"}, text: "アラート")
    end

    context "第2引数に文字列を渡した場合" do
      subject do
        builder.event_button(:alert, "ないよう")
      end

      it "は、渡した文字列を内容に持つボタン要素を返すこと" do
        is_expected.to have_tag("button.event_button", with: {"data-event" => "alert"}, text: "ないよう")
      end
    end

    context "ブロックを渡した場合" do
      subject do
        builder.event_button(:alert) do
          h.concat h.content_tag("div", "(´･ω･｀)", class: "shobon")
          h.concat h.content_tag("span", "（＾ω＾）", class: "boon")
        end
      end

      it "は、ブロックの返り値を内容に持つボタン要素を返すこと" do
        is_expected.to have_tag("button.event_button", with: {"data-event" => "alert"}){
          with_tag("div.shobon", text: "(´･ω･｀)")
          with_tag("span.boon", text: "（＾ω＾）")
        }
      end
    end

    context "第2引数の内容とブロックを両方渡した場合" do
      subject do
        builder.event_button(:alert, "ないよう") do
          h.concat h.content_tag("div", "(´･ω･｀)", class: "shobon")
          h.concat h.content_tag("span", "（＾ω＾）", class: "boon")
        end
      end

      it "は、ブロックの返り値を内容に持つボタン要素を返すこと" do
        is_expected.to have_tag("button.event_button", with: {"data-event" => "alert"}){
          with_tag("div.shobon", text: "(´･ω･｀)")
          with_tag("span.boon", text: "（＾ω＾）")
        }
      end
    end

    context "第2引数にハッシュでHTML属性を渡した場合" do
      subject do
        builder.event_button(:alert, id: "my-id", class: "my-class", data: {hoge: "HOGE"})
      end

      it "は、渡したHTML属性を持つボタン要素を返すこと" do
        is_expected.to have_tag("button#my-id.my-class.event_button", with: {"data-event" => "alert", "data-hoge" => "HOGE"}, text: "アラート")
      end
    end

    context "第2引数に内容を、第3引数にHTML属性を渡した場合" do
      subject do
        builder.event_button(:alert, "ないよう", id: "my-id", class: "my-class", data: {hoge: "HOGE"})
      end

      it "は、渡した内容とHTML属性を持つボタン要素を返すこと" do
        is_expected.to have_tag("button#my-id.my-class.event_button", with: {"data-event" => "alert", "data-hoge" => "HOGE"}, text: "ないよう")
      end
    end

    context "オプション`:highlight`に`true`を渡した場合 (default)" do
      it "は、操作対象をハイライトする指標を持つボタン要素を返すこと" do
        is_expected.to have_tag(".event_button.event_button-highlightable", with: {"data-event" => "alert"})
      end
    end

    context "オプション`:highlight`に`false`を渡した場合" do
      subject do
        builder.event_button(:alert, highlight: false)
      end

      it "は、操作対象をハイライトする指標を持たないボタン要素を返すこと" do
        is_expected.to have_tag(".event_button:not(.event_button-highlightable)", with: {"data-event" => "alert"})
      end
    end
  end
end
