require 'rails_helper'

feature "定義したイベント", js: true do
  before do
    visit root_path
  end

  let(:button){ find(".event_button[data-event='event1']") }

  context "「イベント1」ボタンをクリックすると" do
    scenario "操作対象要素の内容を変えること" do
      expect{ button.click }.to change{ find("#element-of-action-1")['innerHTML'] }.from("操作対象").to("(´･ω･｀)")
    end
  end
end
