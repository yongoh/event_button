require 'rails_helper'

feature "操作対象ハイライト機能", js: true do
  before do
    visit root_path
  end

  let(:button){ find(".event_button[data-event='event1']") }

  context "「イベント1」ボタンをマウスオーバーすると" do
    before do
      button.trigger(:mouseover)
    end

    scenario "操作対象要素をハイライトすること" do
      expect(page).to have_selector("#element-of-action-1.event_button-highlight")
    end

    context "マウスアウトすると" do
      before do
        button.trigger(:mouseout)
      end

      scenario "ハイライトを解除すること" do
        expect(page).not_to have_selector("#element-of-action-1.event_button-highlight")
        expect(page).to have_selector("#element-of-action-1:not(.event_button-highlight)")
      end
    end
  end
end
